//
//  ViewController.swift
//  CountDown
//
//  Created by Jean-Christophe Bremond on 18/12/2016.
//  Copyright © 2016 Jean-Christophe Bremond. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var compte :Int = 0
    
    var minutes: Int=0
    var secondes: Int=0
    var heures: Int=0
    var fractions: Int=0
    
    var startStopWatch: Bool = false
    
    
    var timer = Timer()
   
    @IBOutlet weak var stopButton: UIButton!
    
    @IBOutlet weak var playButton: UIButton!
    
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var minutesSlider: UISlider!
    
    @IBOutlet weak var secondesSlider: UISlider!
    
    
    @IBAction func minutesSliderAction(_ sender: Any) {
        
        updateLabel()
        
    }
    
    @IBAction func secondesSliderAction(_ sender: Any) {
        
        updateLabel()
        
    }
    
    
    @IBAction func playAction(_ sender: Any) {
        
        //timer.fire()
        if (startStopWatch == false)
        {
           // timer = Timer.scheduledTimer(withTimeInterval: <#T##TimeInterval#>, repeats: <#T##Bool#>, block: <#T##(Timer) -> Void#>)
            fractions = ( (Int(minutesSlider.value) * 60) + Int(secondesSlider.value) ) // * 100
            timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updateStopWatch), userInfo: nil, repeats: true)
            //timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateStopWatch), userInfo: nil, repeats: true)
            startStopWatch = true
        }
        else
        {
            timer.invalidate()
            startStopWatch = false
            
        }
    }
    
    func updateLabel()
    {
    
        let lesMinutes = Int(minutesSlider.value)
        let lesSecondes = Int(secondesSlider.value)
        
        timerLabel.text = "\(lesMinutes):\(lesSecondes)"
    }
    
    func reinitLabel() {
        
        timerLabel.text = "00:00"
        startStopWatch = false
    }
    
    func updateStopWatch()
    {
    //test commit
        fractions -= 1
        
        if (fractions==0) {
            secondes-=1
            fractions=0
        }
        
        if (secondes==60) {
           minutes-=1
           secondes = 0
        }
        
        let fractionString = fractions>9 ? "\(fractions)" : "0\(fractions)"
    
        let secondesString = secondes>9 ? "\(secondes)" : "0\(secondes)"
        
        let minutesString = minutes>9 ? "\(minutes)" : "0\(minutes)"
        
        timerLabel.text = "\(minutesString):\(secondesString).\(fractionString)"
        //timerLabel.text = "\(minutesString):\(secondesString)"
        
    }
    
    @IBAction func stopAction(_ sender: Any) {
        
        timer.invalidate()
        // Maj de l'interface
        reinitLabel()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        secondesSlider.value=0
        minutesSlider.value=0
        
        reinitLabel()
        
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

